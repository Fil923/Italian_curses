"use strict";
var bestemmia = "";
var index = 0;
var jsonFile = {};

//Get Function
function _Get(yourUrl) {
  var Httpreq = new XMLHttpRequest(); // a new request
  Httpreq.open('GET', yourUrl, false);
  Httpreq.send(null);
  return Httpreq.responseText;
}

//RandomGenerator of ID
function _getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function _speak(text, callback) {
  var u = new SpeechSynthesisUtterance();
  if (!u) {
    return false;
  }

  // else browser support
  u.text = text;
  u.lang = 'it-IT';

  u.onend = function () {
    if (callback) {
      callback();
    }
  };

  u.onerror = function (e) {
    if (callback) {
      callback(e);
    }
  };

  speechSynthesis.speak(u);
}

function _normalCurse() {

  //Json
  jsonFile = JSON.parse(_Get('./assets/data/saints.json'));
  var saintsNumber = _getRandomInt(0, jsonFile.saints.length - 1);
  var indexPorcus = _getRandomInt(0, jsonFile.porcusMale.length - 1);

  if (index < 10) {
    if (jsonFile.saints[saintsNumber].gender === 'M') {
      bestemmia = jsonFile.porcusMale[indexPorcus].name + ' ' + jsonFile.saints[saintsNumber].name + ' ' + jsonFile.maleCurses[_getRandomInt(0, jsonFile.maleCurses.length - 1)].name;
    } else {
      bestemmia = jsonFile.porcusFemale[indexPorcus].name + ' ' + jsonFile.saints[saintsNumber].name + ' ' + jsonFile.femaleCurses[_getRandomInt(0, jsonFile.femaleCurses.length - 1)].name;
    }
    index++;
  } else {
    bestemmia = jsonFile.specialOne[_getRandomInt(0, 10)].name;
    index = 0;
  }

}

function program() {
  //Click Function
  document.getElementById('curseButton').addEventListener('click', function () {
    _normalCurse();
    document.getElementById('placeHere').innerHTML = bestemmia;

    if (document.getElementById('checkBoxSpeak').checked) {
      _speak(bestemmia);
    }
  });
}

program();
