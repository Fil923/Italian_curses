import {
  shallowMount
} from "@vue/test-utils";
import Button from "@/components/Button.vue";

describe("Button.vue", () => {
  //msgLabel
  it("renders props.msgLabel when passed", () => {
    const msgLabel = "new message";
    const wrapper = shallowMount(Button, {
      propsData: {
        msgLabel
      }
    });
    expect(wrapper.text()).toMatch(msgLabel);
  });
  //msgButton
  it("renders props.msgButton when passed", () => {
    const msgButton = "Button message";
    const wrapper = shallowMount(Button, {
      propsData: {
        msgButton
      }
    });
    expect(wrapper.text()).toMatch(msgButton);
  });
  //data text
});
